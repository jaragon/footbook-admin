<?php

use Illuminate\Database\Seeder;
use App\Item;

class ItemsTableSeeder extends Seeder {

    public function run()
    {

		// Straps: type = 1
		$straps = [
			"/assets/straps/strap-00.svg",
			"/assets/straps/strap-01.svg",
			"/assets/straps/strap-02.svg"
		];

		foreach ($straps as $key => $strap) {
			Item::create([
				'type' => 1,
				'item_id' => 'strap-' . $key,
				'texture_path' => $strap
			]);
		}

		// Patterns: type = 3
		$patterns = [
			"/assets/patterns/bright_squares.png",
			"/assets/patterns/congruent_pentagon.png",
			"/assets/patterns/green_gobbler.png",
			"/assets/patterns/restaurant.png",
			"/assets/patterns/seamless_paper_texture.png",
			"/assets/patterns/skulls.png",
			"/assets/patterns/stardust.png",
			"/assets/patterns/upfeathers.png"
		];

		foreach ($patterns as $key => $pattern) {
			Item::create([
				'type' => 3,
				'item_id' => 'print-design-' . $key,
				'texture_path' => $pattern
			]);
		}

		// Accessories: type = 4
		$accessories = [
			"/assets/accessory/1.png",
			"/assets/accessory/2.png",
			"/assets/accessory/3.png"
		];

		foreach ($accessories as $key => $accessory) {
			Item::create([
				'type' => 4,
				'item_id' => 'accessory-' . $key,
				'texture_path' => $accessory
			]);
		}


    }

}
