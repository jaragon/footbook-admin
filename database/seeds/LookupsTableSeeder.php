<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Lookups;

class LookupsTableSeeder extends Seeder {

	public function run()
	{

		DB::table('lookups')->truncate();
		Model::unguard();
		/*
		foreach(range(1, 3) as $index)
		{
			Lookups::create([
				'key' => 'industry_type',
				'key_id' => $index,
				'value' => 'Option ' . $index
				]);
		}
		*/

		// Item type
		Lookups::create([ 'key'=>'item_type', 'key_id'=>1, 'value'=>'Strap' ]);
		Lookups::create([ 'key'=>'item_type', 'key_id'=>2, 'value'=>'Sole' ]);
		Lookups::create([ 'key'=>'item_type', 'key_id'=>3, 'value'=>'Print Design' ]);
		Lookups::create([ 'key'=>'item_type', 'key_id'=>4, 'value'=>'Accessory' ]);

		// Item properties
		Lookups::create([ 'key'=>'material_type', 'key_id'=>1, 'value'=>'Type Of Material' ]);
		Lookups::create([ 'key'=>'sole', 'key_id'=>1, 'value'=>'Sole' ]);
		Lookups::create([ 'key'=>'vacuum_print', 'key_id'=>1, 'value'=>'Vacuum Print' ]);

		// Age Demographics
		Lookups::create([ 'key'=>'age_demographic', 'key_id'=>1, 'value'=>'Adult' ]);
		Lookups::create([ 'key'=>'age_demographic', 'key_id'=>2, 'value'=>'Teens' ]);
		Lookups::create([ 'key'=>'age_demographic', 'key_id'=>3, 'value'=>'Children' ]);

		// Age Range
		Lookups::create([ 'key'=>'age_range', 'key_id'=>1, 'value'=>'50 yrs and up' ]);
		Lookups::create([ 'key'=>'age_range', 'key_id'=>2, 'value'=>'30-49 yrs' ]);
		Lookups::create([ 'key'=>'age_range', 'key_id'=>3, 'value'=>'20-29 yrs' ]);
		Lookups::create([ 'key'=>'age_range', 'key_id'=>4, 'value'=>'10-19 yrs' ]);
		Lookups::create([ 'key'=>'age_range', 'key_id'=>5, 'value'=>'Children' ]);

		// Mode of purchase
		Lookups::create([ 'key'=>'payment_type', 'key_id'=>1, 'value'=>'Cash' ]);
		Lookups::create([ 'key'=>'payment_type', 'key_id'=>2, 'value'=>'Creditcard' ]);
	}

}
