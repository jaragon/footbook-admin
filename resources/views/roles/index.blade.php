@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
            {!! Breadcrumbs::render() !!}
			<div class="panel panel-default">
				<div class="panel-heading">ROLE ACCESS MANAGEMENT</div>

				<div class="panel-body">
                    <table class="table table-responsive">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th></th>
                        </tr>

                        @foreach ($roles as $role)
                        <tr>
                            <td>{{ $role['id']  }}</td>
                            <td>{{ $role['name']  }}</td>
                            <td class="text-right">
                                <a href="{{ route('admin.roles.show', [$role['id']]) }}" class="btn btn-default btn-xs">View</a>
                                <a href="{{ route('admin.roles.edit', [$role['id']]) }}" class="btn btn-default btn-xs">Edit</a>
                                <a href="{{ route('admin.roles.destroy', [$role['id']]) }}" class="btn btn-default btn-xs btn-danger">Delete</a>
                            </td>
                        </tr>
                        @endforeach

                    </table>
                    <div class="pull-right">
                        <a href="{{ route('admin.permissions.edit') }}" class="btn btn-primary">Manage permissions</a>
                        <a href="{{ route('admin.roles.create') }}" class="btn btn-primary">Add Role</a>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
