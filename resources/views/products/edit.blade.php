@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">EDIT PRODUCT</div>

				<div class="panel-body">
					{!! Breadcrumbs::render() !!}

                    @include('errors.list')
                    {!! Form::model($product, [ 'method' => 'PATCH', 'files'=>'true', 'route' => [ 'admin.products.update', $product["id"] ], 'class' => 'form-horizontal']) !!}
						@include('products.form')
						<div class="col-md-4 col-md-offset-4">
					        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
					    	<a href="{{ route('admin.products.show', [$product['id']]) }}" class="btn btn-primary">Cancel</a>
					    </div>
                    {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
