@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render() !!}
                <div class="panel panel-default">
                    <div class="panel-heading">EDIT PRODUCT</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Slipper:</h4>
                                <p class="text-center">
                                    <img src="{{ asset($product['product_path']) }}" style="max-width: 100%;">
                                </p>
                            </div>
                            <div class="col-md-6">
                                <h4>Product ID: {{ $product['id'] }}</h4>
                                <h4>Product properties:</h4>
                                <dl class="dl-horizontal">
                                    @foreach ($product->properties as $property)
                                        <dt>{{ $property->key }}</dt>
                                        <dd>{{ $property->value }}</dd>
                                    @endforeach
                                </dl>
                                <h4>Items:</h4>
                                <dl class="dl-horizontal">
                                    @foreach ($product->items as $item)
                                        <dt>{{ $item->type_name }}</dt>
                                        <dd>
                                            <a href="{{ route('admin.items.show', [$item->id]) }}">
                                                {{ $item->item_id }}
                                            </a>
                                        </dd>
                                    @endforeach
                                </dl>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <a href="{{ route('admin.products.edit', [$product]) }}" class="btn btn-primary">Edit</a>
                                <a href="{{ route('admin.products.index') }}" class="btn btn-primary">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
