<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Footbook Admin</title>

	<link href="{{ asset('/admin-assets/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/admin-assets/css/admin.css') }}" rel="stylesheet">

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="{{ asset('/admin-assets/js/bootbox.min.js') }}"></script>
	<script src="{{ asset('/admin-assets/js/admin.js') }}"></script>

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/admin"><img src="/images/logo.png" alt="" style="max-height: 40px; position: relative; top: -8px;"/></a>
			</div>

			<style type="text/css">
				.dropdown-menu > li > a {
					font-weight: 100;
				}
			</style>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				@if (!Auth::guest())
				<ul class="nav navbar-nav">
					<li>
						<a href="{{ route('admin.products.index') }}">
							Products
						</a>
					</li>
					<li class="dropdown"><a href="{{ route('admin.items.index') }}" class="dropdown-toggle" data-toggle="dropdown">Items <b class="caret"></b></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ route('admin.items.index') }}">View Items</a></li>
							<li><a href="{{ route('admin.items.create') }}">Add Item</a></li>
						</ul>
					</li>
					<li class="dropdown"><a href="{{ route('admin.themes.index') }}" class="dropdown-toggle" data-toggle="dropdown">Themes <b class="caret"></b></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ route('admin.themes.index') }}">View Themes</a></li>
							<li><a href="{{ route('admin.themes.create') }}">Add Themes</a></li>
						</ul>
					</li>
					<li class="dropdown"><a href="{{ route('admin.users.index') }}" class="dropdown-toggle" data-toggle="dropdown">Users <b class="caret"></b></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ route('admin.users.index') }}">View Users</a></li>
							<li><a href="{{ route('admin.users.create') }}">Add Users</a></li>
							<li class="divider"></li>
							<li><a href="{{ route('admin.roles.index') }}">View Roles</a></li>
							<li><a href="{{ route('admin.roles.create') }}">Add Roles</a></li>
							<li><a href="{{ route('admin.permissions.edit') }}">Edit Permissions</a></li>
						</ul>
					</li>
				</ul>
				@endif

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/admin/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/admin/auth/register') }}">Register</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->email }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/admin/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')

</body>
</html>
