@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			{!! Breadcrumbs::render() !!}
			<div class="panel panel-default">
				<div class="panel-heading">EDIT THEME</div>

				<div class="panel-body">

                    @include('errors.list')

                    {!! Form::model($theme, [ 'method' => 'PATCH', 'files'=>'true', 'route' => [ 'admin.themes.update', $theme["id"] ], 'class' => 'form-horizontal']) !!}

						@include('themes.form')

						<div class="col-md-4 col-md-offset-4">
					        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
					    	<a href="{{ route('admin.themes.show', [$theme['id']]) }}" class="btn btn-primary">Cancel</a>
					    </div>
                    {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
