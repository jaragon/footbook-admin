<?php namespace App\Http\Controllers;

use App\Item;
use App\ItemProperty;
use App\Theme;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Contracts\Auth\Guard;

use Illuminate\Http\Request;
use Storage;
use Image;

class ItemsController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$items = Item::paginate(10);

		return view('items.index', compact('items'));
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function search(Request $request)
	{
		$query = $request->input('q');
		$items = Item::search($query)->paginate(10);
		$request->flash();

		return view('items.index', compact('items', 'query'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['itemTypesList'] = $this->lookupSelect('item_type');
		$data['themesList'] = Theme::get()->lists('name', 'id');
		$data['itemThemes'] = null;
		return view('items.create', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param StoreItemRequest
	 * @return Response
	 */
	public function store(Requests\StoreItemRequest $request)
	{
		$input = $request->all();

		$item = Item::create($input);

		if (isset($input['themes']))
			$item->themes()->sync($input['themes']);

		// Save image and get thumbnail
		// todo: set thumbnail based on item type
		if ($texture = array_pull($input, 'texture')) {
			$folder = '/uploads/items/textures/';
			$filename = $item->id . '.' . $texture->getClientOriginalExtension();
			$path = $folder . $filename;
			// if ($texture->getClientOriginalExtension() != 'svg') {
			// 	$thumbnail = Image::make($texture)->fit(50)
			// 						->save(public_path($folder) . 'thumbnail_' . $item->id . '.jpg');
			// }
			$texture->move(public_path($folder), $filename);

			$item->texture_path = $path;
			$item->save(); // Update item with path
		}

		if (isset($input['properties'])) {
			foreach ($input['properties'] as $key => $value) {
				$item->properties()->save(new ItemProperty([
					'key' => $key,
					'value' => $value
				]));
			}
		}

		return redirect()->route('admin.items.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$item = Item::find($id)->load(['properties', 'themes']);
		return view('items.show', compact('item'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['itemTypesList'] = $this->lookupSelect('item_type');
		$data['themesList'] = Theme::all()->lists('name', 'id');
		$item = Item::find($id)->load(['properties', 'themes']);

		$data['itemThemes'] = $item->themes->lists('id');

		$item = $item->toArray();
		// Unserialize properties
		foreach (array_pull($item, 'properties') as $property) {
			$item['properties'][$property['key']] = $property['value'];
		}

		$data['item'] = $item;

		return view('items.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Requests\UpdateItemRequest $request)
	{
		$item = Item::with(['properties', 'themes'])->find($id);
		$input = $request->all();

		// Save image and get thumbnail
		// todo: set thumbnail based on item type
		if ($texture = array_pull($input, 'texture')) {
			$folder = '/uploads/items/textures/';
			$filename = $item->id . '.' . $texture->getClientOriginalExtension();
			$path = $folder . $filename;
			// if ($texture->getClientOriginalExtension() != 'svg') {
			// 	$thumbnail = Image::make($texture)->fit(50)
			// 						->save(public_path($folder) . 'thumbnail_' . $item->id . '.jpg');
			// }
			$texture->move(public_path($folder), $filename);

			$item->texture_path = $path;
			$item->save(); // Update item with path
		}

		if (isset($input['themes']))
			$item->themes()->sync($input['themes']);

		$item->update($input);

		if (!$item->properties->isEmpty())
			$item->properties()->delete();

		if (isset($input['properties'])) {
			foreach ($input['properties'] as $key => $value) {
				$item->properties()->save(new ItemProperty([
					'key' => $key,
					'value' => $value
				]));
			}
		}

		return redirect()->route('admin.items.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Item::find($id)->delete();

		return redirect()->route('admin.items.index');
	}

}
