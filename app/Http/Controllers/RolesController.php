<?php namespace App\Http\Controllers;

use App\Role;
use App\Permission;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Contracts\Auth\Guard;

use Illuminate\Http\Request;

class RolesController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roles = Role::get()->toArray();

		return view('roles.index', compact('roles'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$role = null;
		return view('roles.create', compact('role'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$role = Role::create($request->all());

		return redirect()->route('admin.roles.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$role = Role::find($id);

		return view('roles.show', compact('role'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$role = Role::find($id)->toArray();

		return view('roles.edit', compact('role'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$role = Role::find($id);
		$role->update($request->all());

		return redirect()->route('admin.roles.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Role::find($id)->delete();

		return redirect()->route('admin.roles.index');
	}


	/**
	 * List all permissions
	 *
	 * @return Response
	 */
	public function editPermissions()
	{
		$roles = Role::all()->load('perms');
		$permissions = Permission::all();

		return view('roles.permissions', compact('roles', 'permissions'));
	}

	public function updatePermissions(Request $request)
	{
		foreach ($request['permissions'] as $role => $permissions) {
			Role::whereName($role)->first()->perms()->sync($permissions);
		}

		return redirect()->back()->with('message', 'Successfully updated role permissions')
								->with('alert-class', 'success');
	}

}
