<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::group(['prefix'=>'admin'], function(){

	Route::get('/', function() {
		if (Auth::guest()) {
			return redirect()->to('/admin/auth/login');
		}

		else {
			return redirect()->route('admin.dashboard');
		}
	});

	Route::get('dashboard', ['uses'=>'HomeController@admin', 'as'=>'admin.dashboard']);

	Route::get('/items/search', ['uses'=>'ItemsController@search', 'as'=>'admin.items.search']);
	Route::resource('items', 'ItemsController');
	Route::get('/items/{id}/delete', ['uses'=>'ItemsController@destroy', 'as'=>'admin.items.destroy']);

	Route::resource('users', 'UsersController');
	Route::get('/users/{id}/delete', ['uses'=>'UsersController@destroy', 'as'=>'admin.users.destroy']);

	Route::get('/themes/search', ['uses'=>'ThemesController@search', 'as'=>'admin.themes.search']);
	Route::resource('themes', 'ThemesController');
	Route::get('/themes/{id}/delete', ['uses'=>'ThemesController@destroy', 'as'=>'admin.themes.destroy']);

	Route::resource('products', 'ProductsController');
	Route::get('/products/{id}/delete', ['uses'=>'ProductsController@destroy', 'as'=>'admin.products.destroy']);

	Route::get('/roles/permissions', ['uses'=>'RolesController@editPermissions', 'as'=>'admin.permissions.edit']);
	Route::post('/roles/permissions', ['uses'=>'RolesController@updatePermissions', 'as'=>'admin.permissions.update']);
	Route::resource('roles', 'RolesController');

	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);


});

Route::post('/products/save', ['uses'=>'ProductsController@store', 'as'=>'products.store']);
